#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matrix.h"

int main(int argc, char* argv[]) {

    (void) argc;
    (void) argv;

    Matrix* m;
    
    unsigned int h = 4;
    
    m = new_matrix(h);

    matrix_set(m, 0, 0, 1);
    matrix_set(m, 0, 1, 0);
    matrix_set(m, 0, 2, 2);
    matrix_set(m, 0, 3, 0);     //Remplace 3 par 1
    matrix_set(m, 1, 0, 0);
    matrix_set(m, 1, 1, 3);
    matrix_set(m, 1, 2, 0);
    matrix_set(m, 1, 3, 0);     //Ne doit pas afficher le 0
    matrix_set(m, 2, 0, 0);
    matrix_set(m, 2, 1, 0);
    matrix_set(m, 2, 2, 0);
    matrix_set(m, 2, 3, 5);
    matrix_set(m, 3, 0, 0);
    matrix_set(m, 3, 1, 4);
    matrix_set(m, 3, 2, 0);
    matrix_set(m, 3, 3, 0);
    
    printf("\n");
    printf("Matrix affichage.\n");
    Matrix_display(m);
    
    printf("\n");
    printf("Matrix multiplication.\n");
    Matrix_mult(m,m);
    
    Matrix_destroy(m);

    return 0;

}
