#include "matrix.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct MatrixElement MatrixElement; 

struct Matrix {
    unsigned int width;
    unsigned int height;
    MatrixElement* row_elements;
    MatrixElement* column_elements;
};

struct MatrixElement {
    double value;
    unsigned int row_index;
    unsigned int column_index;
    MatrixElement* row_ptr_prev;
    MatrixElement* row_ptr_next;
    MatrixElement* column_ptr_prev;
    MatrixElement* column_ptr_next;
};


Matrix* new_matrix(unsigned int dimension) {

    if (dimension == 0) {
        //Bad parameters
        return NULL;
        exit(EXIT_FAILURE);
    }
     
    Matrix* m = malloc(sizeof(Matrix));
    MatrixElement* elt = malloc(sizeof(MatrixElement));
    
    if(m == NULL || elt == NULL) {
        return NULL;
        exit(EXIT_FAILURE);
    }
    
    //add the table and the size of m
    m->height = dimension;
    m->width = dimension;
    
    elt->value = 0;
    elt->row_index = dimension;
    elt->column_index = dimension;
    
    elt->row_ptr_prev = elt;
    elt->row_ptr_next = elt;
    elt->column_ptr_prev = elt;
    elt->column_ptr_next = elt;
    
    m->row_elements = elt;
    m->column_elements = elt;

    return m;
}


/**************************************************************************************************/
void matrix_set(Matrix* m, unsigned int i, unsigned int j, double value) {
     
    // On le mémorise pour pouvoir faire des comparaisons de pointeurs
    MatrixElement* dummy_element = m->row_elements;
    
    // next_in_row est l'élément avant lequel insérer le nouvel élément
    
    MatrixElement* next_in_row = m->row_elements->row_ptr_next;
    // comparaisons
    
    printf("Passage....\n");
    
    while(next_in_row != dummy_element && (next_in_row->row_index < i 
                                           || (next_in_row->row_index == i 
                                               && next_in_row->column_index < j))) {
        printf("Passing over (%d, %d) = %f\n",
        next_in_row->row_index,
        next_in_row->column_index,
        next_in_row->value);
        next_in_row = next_in_row->row_ptr_next;
    }
     
    // On est au bon endroit
    
    if((next_in_row->row_index == i  && next_in_row->column_index == j) || value == 0) {
        // L'élément existe déjà
        next_in_row->value = value;
    } 
    else {
        
        // Insérer
        MatrixElement* new_element = malloc(sizeof(MatrixElement));
        if (new_element == NULL) {
            printf("printf Impossible d'allouer la mémoire pour la création de la matrice.\n");
            exit(EXIT_FAILURE);
        }

        // Initialisation le nouvel élément
        new_element->row_ptr_next = next_in_row;
        new_element->row_ptr_prev = next_in_row->row_ptr_prev;
         
        // Insertion des valeurs
        new_element->row_ptr_next->row_ptr_prev = new_element;
        new_element->row_ptr_prev->row_ptr_next = new_element;

        new_element->value = value;

        new_element->row_index = i;
        new_element->column_index = j;

    }

}

/**************************************************************************************************/
void Matrix_destroy(Matrix* m) {
    
    if (m == NULL) {
        exit(EXIT_FAILURE);
    }
    
    MatrixElement* dummy_element = m->row_elements;
    MatrixElement *nextElt = dummy_element->row_ptr_next;
    
    while(nextElt != dummy_element) {
        nextElt = nextElt->row_ptr_next;
        free(nextElt);
    }
    //
    free(m);
}

/**************************************************************************************************/
void Matrix_display(Matrix* m) {

    if (m == NULL) {
        exit(EXIT_FAILURE);
    }
    
    MatrixElement *dummy_element = m->row_elements;
    MatrixElement *nextElt = dummy_element->row_ptr_next;
    printf("- Matrix ---------------\n");
    while(nextElt != dummy_element){
        printf("(%d, %d) -> %f\n",nextElt->row_index,nextElt->column_index,nextElt->value);
        nextElt = nextElt->row_ptr_next;
    }
    printf("------------------------\n");
    
}

/**************************************************************************************************/
void Matrix_mult(Matrix* A, Matrix* B) {
    
    printf("Allocation mémoire et création de la matrice.\n");
    
    //Vérifier les dimensions
    if((A->width != B->width) || (A->height != B->height)){
        printf("Taille de la matrice différente, multiplication impossible.\n");
        exit(EXIT_FAILURE);
    }
    
    //Créer le résultat R 
    Matrix* resultat = malloc(sizeof(Matrix));
    if (resultat == NULL) {
        printf("Erreur d'allocation mémoire.\n");
        exit(EXIT_FAILURE);
    }
    
    MatrixElement* dummy_element_A = A->row_elements;
    MatrixElement* dummy_element_B = B->row_elements;
 
    int row_elements[A->height];
    int results[B->width];
    unsigned int j;
 
    //Parcourt les lignes de A 
    MatrixElement* current_element_A = dummy_element_A->row_ptr_next;
    
    //Parcourt les colonnes de B
    MatrixElement* current_element_B = dummy_element_B->column_ptr_next;        
        
    printf("Entre dans la boucle....\n\n");
    
    while(current_element_A != dummy_element_A) { 
        
        printf("----itération---\n");
        
        for(j = 0; j < A->height; j++) {
            row_elements[j] = 0;
            results[j] = 0;
        }
        
        printf("On parcourt les lignes de la matrice.\n\n");
        
        unsigned int current_line = current_element_A->row_index;
        
        // On retient les éléments de la ligne pour ne pas la parcourir à chaque fois
        while(current_element_A != dummy_element_A && current_element_A->row_index == current_line) {
            row_elements[current_element_A->column_index] = current_element_A->value;
            current_element_A = current_element_A->row_ptr_next;
            printf("printf Ligne %d\n",current_element_A->row_index);
        }
 
        printf("On parcourt les colonnes de la matrices.\n\n");
        
        // On parcourt les colonnes de B pour les multiplier avec la ligne de A
        while(current_element_B != dummy_element_B) {
            printf("printf ligne%d\n",current_element_B->row_index);
            printf("printf colonne%d\n",current_element_B->column_index);
            current_element_B = dummy_element_B->column_ptr_next;
            results[current_element_B->column_index] += row_elements[current_element_B->row_index] * current_element_B->value;
            //Vérifier les résultats
            printf("résultat %d\n",results[j]);
        }
 
        printf("Affichage des résultats\n\n");
        
        // On insère les éléments
        for(j = 0; j < A->height; j++) {
            if(results[j] != 0) {
                matrix_set(resultat, current_line, j, results[j]);
            }
        }
    }
    
    for(j = 0; j < A->height; j++) {
        printf("result : %d\n",results[j]);
    }
}
