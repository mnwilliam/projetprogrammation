all: main.o matrix.o
	gcc -o main main.o matrix.o
    
main.o: main.c matrix.h
	gcc -o main.o -c main.c -g --std=c99 --pedantic -Wall -W -Wmissing-prototypes
    
matrix.o: matrix.c matrix.h
	gcc -o matrix.o -c matrix.c -g --std=c99 --pedantic -Wall -W -Wmissing-prototypes
    
clean:
	rm -f main *.o
	
memory:
	valgrind --track-origins=yes --leak-check=full ./main
