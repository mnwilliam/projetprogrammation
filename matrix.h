#ifndef _MATRIX_H_
#define _MATRIX_H_

typedef struct Matrix Matrix;

/*
* Create matrix
*/
Matrix* new_matrix(unsigned int dimension);

/*
* Retriev elements of matrix
*/
void matrix_set(Matrix* m, unsigned int i, unsigned int j, double value);

/*
* Delete element
*/
void Matrix_destroy(Matrix* m);

/*
* display the matrix
*/
void Matrix_display(Matrix* m);

/*
* multiplication the matrix
*/
void Matrix_mult(Matrix* A, Matrix* B);

#endif


